define([
	'require',
	'madjoh_modules/ajax/ajax',
	'madjoh_modules/msgbox/msgbox',
	'madjoh_modules/profile/profile'
],
function(require, AJAX, MsgBox, Profile){
	var Account = {
		// VERIFICATION
			sendMailVerification : function(mail, forceResend){
				var parameters = {mail : mail};
				if(forceResend) parameters.forceResend = forceResend;
				return AJAX.post('/user/verify/mail/send', {parameters : parameters, noAuth : true, loader : true, sync : false});
			},
			verifyMail : function(mail, code){
				var parameters = {mail : mail, code : code};
				return AJAX.post('/user/verify/mail', {parameters : parameters, noAuth : true, loader : true, sync : false});
			},
			sendSMSVerification : function(phone, forceResend){
				var parameters = {phone_number : phone};
				if(forceResend) parameters.forceResend = forceResend;
				return AJAX.post('/user/verify/sms/send', {parameters : parameters, noAuth : true, loader : true, sync : false});
			},
			verifySMS : function(phone, code){
				var parameters = {phone_number : phone, code : code};
				return AJAX.post('/user/verify/sms', {parameters : parameters, noAuth : true, loader : true, sync : false});
			},
			verifySlug : function(slug){
				var parameters = {slug : slug};
				return AJAX.post('/user/verify/slug', {parameters : parameters, noAuth : true});
			},
			getNewSlug : function(slug){
				var parameters = {slug : slug};
				return AJAX.post('/user/verify/slug/suggest', {parameters : parameters, noAuth : true});
			},

		// CREATE / LOGIN
			isMail : function(mail){
				var mailRegEx = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
				return mailRegEx.test(mail)
			},
			checkInputs : function(parameters){
				for(var key in parameters) if(!parameters[key] || parameters[key].length === 0) return false;

				if(parameters.mail && !Account.isMail(parameters.mail)) return false;

				return true;
			},
			create : function(parameters, photo){
				if(!Account.checkInputs(parameters)){
					MsgBox.show('inputs_error');
					return Promise.reject('inputs_error');
				}

				return AJAX.file('/user/create', {parameters : parameters, file : photo, fileName : 'photo', loader : true, noAuth : true});
			},
			login : function(creds, password){
				var parameters = {password 	: password};

				var key = Account.isMail(creds) ? 'mail' : 'slug';
				parameters[key] = creds;

				if(!Account.checkInputs(parameters)){
					MsgBox.show('inputs_error');
					return Promise.reject('inputs_error');
				}

				return AJAX.post('/user/login', {parameters : parameters, loader : true, noAuth : true});
			},

		// FORGOT
			forgot : function(mail){
				var parameters = {mail : mail};
				return AJAX.post('/user/forgot/password', {parameters : parameters, loader : true, noAuth : true});
			},

		// TIME
			recordTime : function(time_delay){
				if(time_delay){
					var now = new Date().getTime();
					localStorage.setItem('com.madjoh.time_delay', now - time_delay);
					return;
				}

				AJAX.post('/user/get/time').then(function(result){
					var now = new Date().getTime();
					localStorage.setItem('com.madjoh.time_delay', now - result.data.now);
				});
			},

		// UPDATE
			update : function(data, photo){
				return AJAX.file(
					'/user/update',
					{
						parameters 	: data,
						file 		: photo,
						fileName 	: 'photo'
					}
				);
			},
			updatePassword : function(password_old, password_new){
				var parameters = {
					password_old 	: password_old,
					password_new 	: password_new
				};

				return AJAX.post('/user/update/password', {parameters : parameters, loader : true});
			},

		// REMOVE
			remove : function(){
				return new Promise(function(resolve, reject){
					MsgBox.show('remove', {onOk : function(){
						Account.removeConfirm().then(resolve, resolve);
					}});
				});
			},
			removeConfirm : function(){
				return Promise.resolve();
				return AJAX.post('/user/remove');
			},

		// LABEL
			requestLabel : function(description, links){
				var parameters = {
					description : description,
					links 		: JSON.stringify(links)
				};
				return AJAX.post('/user/label/request', {parameters : parameters});
			},
	};

	return Account;
});