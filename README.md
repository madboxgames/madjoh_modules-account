# account #

v2.3.4

(DOC NOT UP TO DATE)

This modules enables signing in, signing up, logging out and resetting password of a MadJoh Account.

## Getting Started ##

- Make sure you meed the following dependencies requirements :
	- AJAX
	- CustomAlert
	- MadJohForm

## You should know about ##

- This module is linked to the [Preferences](http://madjoh.com/developer/#/docs/preferences) module which stores preferences and uploads them to the server.
- This module fires [CustomEvents](http://madjoh.com/developer/#/docs/custom-events) when account creation or loggin are successful. You should add CustomEventListeners to document to catch these events.

To completely master this module you can follow [this tutorial](http://madjoh.com/developer/#/tutorial/account).

## API ##

- [create](#create)
- [login](#login)
- [forgot](#forgot)
- [logout](#logout)


## create ##

** Parameters **

```js
Account.create(mail, password, firstname, lastname, [alerting]);
```

| Parameter 	| Type 		| Default  	| Description 														|
| --------------| --------- | ----------| ----------------------------------------------------------------- |
| mail*			| string 	| 			| The user eMail													|
| password*		| string 	| 			| The user password													|
| firstname*	| string 	| 			| The user firstname												|
| lastname*		| string 	| 			| The user lastname													|
| alerting		| boolean 	| false		| Set to true if you want the module to handle the custom alerts	|

** Events **

If the account creation is sucessfull then two events can be fired :

| Event 		| Description 																	| 
| --------------| ----------------------------------------------------------------------------- |
| NewUser		| The User is new on this application and his account was sucessfully created 	|
| LoggedIn		| The User already had an account on this application and simply logged in 		|

## login ##

** Parameters **

```js
Account.login(mail, password, [alerting]);
```

| Parameter 	| Type 		| Default  	| Description 														|
| --------------| --------- | ----------| ----------------------------------------------------------------- |
| mail*			| string 	| 			| The user eMail													|
| password*		| string 	| 			| The user password													|
| alerting		| boolean 	| false		| Set to true if you want the module to handle the custom alerts	|

** Events **

If the account creation is sucessfull then two events can be fired :

| Event 		| Description 																										| 
| --------------| ----------------------------------------------------------------------------------------------------------------- |
| NewUser		| The User indeed had a MadJoh account but is new on this application. His account has been successfully created 	|
| LoggedIn		| The User successfully logged in 																					|

## forgot ##

** Parameters **

```js
Account.forgot(mail, [callback]);
```

| Parameter 	| Type 		| Default 		 	| Description 																			|
| --------------| --------- | ------------------| ------------------------------------------------------------------------------------- |
| mail*			| string 	| 					| The user eMail																		|
| callback		| function 	| default callback	| A callback to execute when an email has been sent to the user with its new password	|

## logout ##

```js
Account.logout();
```

No parameters here